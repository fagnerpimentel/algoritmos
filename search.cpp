#include <iostream>
#include "include/algoritmos.h"

//#include <boost/tuple/tuple.hpp>
//#include "../gnuplot-iostream/gnuplot-iostream.h"

using namespace std;

Mersenne mersene;

static double diffclock(clock_t begin,clock_t end)
{
    double diffticks=end-begin;
    double diffms=(diffticks)/(CLOCKS_PER_SEC);
    return diffms;
}

int main()
{

//    // gnuplot
//    Gnuplot gp("tee plot.gp | gnuplot -persist");
//    std::vector<std::pair<double, double> > xy_pts_A;
//    std::vector<std::pair<double, double> > xy_pts_B;
//    gp << "set key left top" << std::endl;


    Algoritimos alg = Algoritimos();

    int graph_size = 6;
    int density_percent = 40;
//    for (int graph_size = 1; graph_size < 100; graph_size++) {
//        for (int density_percent = 0; density_percent < 100; density_percent++) {
            int weight_max = graph_size;

            std::vector<Vertex*> G_prim;
            std::vector<Vertex*> G_dijkstra;
            for (int i = 0; i < graph_size; i++) {
                G_prim.push_back(new Vertex(i, INF, NIL));
                G_dijkstra.push_back(new Vertex(i, INF, NIL));
            }

            Graph w = Graph(graph_size);
            for (int i = 0; i < graph_size; i++){
                for (int j = 0; j < graph_size; j++){
                    if(j >= i) continue;
                    if(mersene.myRandom(100) <= density_percent){
                        int value = mersene.myRandom(weight_max);
                        w.setWeight(i,j, value);
                        w.setWeight(j,i, value);
                    }
                }
            }

            int vertex = mersene.myRandom(graph_size);


            /** Prim solution **/
            const clock_t begin_time_prim = clock();
            alg.prim(G_prim,w,vertex);
            const clock_t end_time_prim = clock();
            double time_prim = diffclock(begin_time_prim, end_time_prim);
            //std::cout << "Time Prim: " << time_prim << " s." << endl;
//            xy_pts_A.push_back(std::make_pair(density_percent, time_prim));

            /** Dijkstra solution **/
            const clock_t begin_time_dijkstra = clock();
            alg.dijkstra(G_dijkstra,w,vertex);
            const clock_t end_time_dijkstra = clock();
            double time_dijkstra = diffclock(begin_time_dijkstra, end_time_dijkstra);
            //std::cout << "Time Dijkstra: " << time_dijkstra << " s." << endl;
//            xy_pts_B.push_back(std::make_pair(density_percent, time_dijkstra));

//            gp << "plot" << gp.file1d(xy_pts_A) << "with lines title 'Prim',"
//                         << gp.file1d(xy_pts_B) << "with lines title 'Dijkstra',"
//                         << std::endl;


            Graph mst_prim = Graph(graph_size);
            for(int i = 0; i < graph_size; i++){
                if(G_prim.at(i)->mVertex == -1) continue;
                mst_prim.setWeight(G_prim.at(i)->mID, G_prim.at(i)->mVertex, G_prim.at(i)->mWeight);
                mst_prim.setWeight(G_prim.at(i)->mVertex, G_prim.at(i)->mID, G_prim.at(i)->mWeight);
            }
            Graph mst_dijkstra = Graph(graph_size);
            for(int i = 0; i < graph_size; i++){
                if(G_dijkstra.at(i)->mVertex == -1) continue;
                mst_dijkstra.setWeight(G_dijkstra.at(i)->mID, G_dijkstra.at(i)->mVertex, G_dijkstra.at(i)->mWeight);
                mst_dijkstra.setWeight(G_dijkstra.at(i)->mVertex, G_dijkstra.at(i)->mID, G_dijkstra.at(i)->mWeight);
            }


            w.print_image("graph");
            mst_prim.print_image("mst_prim");
            mst_dijkstra.print_image("mst_dijkstra");

//        }
//    }

//        gp << "set term png nocrop enhanced font 'verdana,8' size 1280,600" << std::endl;
//        gp << "set output 'file.png'" << std::endl;
//        gp << "replot" << std::endl;
//        gp << "set term x11" << std::endl;

    return 0;
}
