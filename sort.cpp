#include <iostream>
#include "include/algoritmos.h"

//#include <boost/tuple/tuple.hpp>
//#include "../gnuplot-iostream/gnuplot-iostream.h"

using namespace std;

Mersenne mersene;

void print_array(string name, vector<int>A){
    cout << name << ": ";
    cout << A[0];
    for (unsigned int i = 1; i < A.size(); i++) {
        cout << ", " << A[i];
    }
    cout << endl;
}

static double diffclock(clock_t begin,clock_t end)
{
    double diffticks=end-begin;
    double diffms=(diffticks)/(CLOCKS_PER_SEC);
    return diffms;
}

int main()
{

    Algoritimos alg = Algoritimos();

//    // gnuplot
//    Gnuplot gp("tee plot.gp | gnuplot -persist");
//    std::vector<std::pair<double, double> > xy_pts_A;
//    std::vector<std::pair<double, double> > xy_pts_B;
//    std::vector<std::pair<double, double> > xy_pts_C;
//    gp << "set key left top" << std::endl;


    int n = 5000;

    cout << "Size of vector:"<< endl;
    cin >> n;
    int i = n;

//    for (int i = 2; i <= n; i++) {
        vector<int> A;
        for (int j = 0; j < i; j++) {
            int rand =  mersene.myRandom(-n,n);
            A.push_back(rand);
        }
        print_array("Array", A);

        /** Quicksort solution **/
        vector<int> A_quicksort = A;
        const clock_t begin_time_quicksort = clock();
        alg.quicksort(A_quicksort, 0, A_quicksort.size()-1);
        const clock_t end_time_quicksort = clock();
        double time_quicksort = diffclock(begin_time_quicksort, end_time_quicksort);
        print_array("Quicksort", A_quicksort);
        std::cout << "Time quicksort: " << time_quicksort << " s." << endl;

        /** Quicksort randon solution **/
        vector<int> A_quicksort_randon = A;
        const clock_t begin_time_quicksort_randon = clock();
        alg.quicksort(A_quicksort_randon, 0, A_quicksort_randon.size()-1);
        const clock_t end_time_quicksort_randon = clock();
        double time_quicksort_randon = diffclock(begin_time_quicksort_randon, end_time_quicksort_randon);
        print_array("Quicksort randon", A_quicksort_randon);
        std::cout << "Time quicksort randon: " << time_quicksort_randon << " s." << endl;

        /** Bubblesort solution **/
        vector<int> A_bubblesort = A;
        const clock_t begin_time_bubblesort = clock();
        alg.bublesort(A_bubblesort);
        const clock_t end_time_bubblesort = clock();
        double time_bubblesort = diffclock(begin_time_bubblesort, end_time_bubblesort);
        print_array("Bubblesort", A_bubblesort);
        std::cout << "Time bubblesort: " << time_bubblesort << " s." << endl;

//        xy_pts_A.push_back(std::make_pair(i, time_quicksort));
//        xy_pts_B.push_back(std::make_pair(i, time_quicksort_randon));
//        xy_pts_C.push_back(std::make_pair(i, time_bubblesort));

//        gp << "plot" << gp.file1d(xy_pts_A) << "with lines title 'Quicksort',"
//                     << gp.file1d(xy_pts_B) << "with lines title 'Quicksort random',"
//                     << gp.file1d(xy_pts_C) << "with lines title 'Bubblesort',"
//                     << std::endl;

//    }



//    gp << "set term png nocrop enhanced font 'verdana,8' size 1280,600" << std::endl;
//    gp << "set output 'file.png'" << std::endl;
//    gp << "replot" << std::endl;
//    gp << "set term x11" << std::endl;

    return 0;
}
