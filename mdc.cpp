#include <iostream>
#include "include/algoritmos.h"

//#include <boost/tuple/tuple.hpp>
//#include "../gnuplot-iostream/gnuplot-iostream.h"

using namespace std;

static double diffclock(clock_t begin,clock_t end)
{
    double diffticks=end-begin;
    double diffms=(diffticks)/(CLOCKS_PER_SEC);
    return diffms;
}

int main()
{

    Algoritimos alg = Algoritimos();

    int a,b;
    cout << "First number:"<< endl;
    cin >> a;
    cout << "Second number:"<< endl;
    cin >> b;

//    const clock_t begin_time_total = clock();

//    // gnuplot
//    Gnuplot gp("tee plot.gp | gnuplot -persist");
//    std::vector<std::pair<double, double> > xy_pts_A;
//    std::vector<std::pair<double, double> > xy_pts_B;

//    int max = 1000;
//    int steps = 1;
//    int count = 0;
//    for (int a = 1; a <= max; a+=steps) {
//        for (int b = 1; b <= max; b+=steps) {
//            count++;
//            cout << (double) count/(max*max/steps)*100 << "%" << endl;

            int bigger = a;
            int lower = b;
            if (a < b){
                bigger = b;
                lower = a;
            }

            /** iteractive solution **/
            const clock_t begin_time_iteractive = clock();
            int r_iteractive = alg.mdc_iteractive(bigger,lower);
            const clock_t end_time_iteractive = clock();
            double time_iteractive = diffclock(begin_time_iteractive, end_time_iteractive);

            cout << "result iteractive: "<< r_iteractive << endl;
            std::cout << "Time iteractive: " << time_iteractive << " s." << endl;

            /** recursive solution **/
            const clock_t begin_time_recursive = clock();
            int r_recursive = alg.mdc_recursive(bigger,lower);
            const clock_t end_time_recursive = clock();
            double time_recursive = diffclock(begin_time_recursive, end_time_recursive);

            cout << "result recursive: "<< r_recursive << endl;
            std::cout << "Time recursive: " << time_recursive << " s." << endl;

            cout << endl;

//            xy_pts_A.push_back(std::make_pair(count, time_iteractive));
//            xy_pts_B.push_back(std::make_pair(count, time_recursive));
//        }
//    }


//    gp << "plot" << gp.file1d(xy_pts_A) << "with lines title 'iteractive',"
//                 << gp.file1d(xy_pts_B) << "with lines title 'recursive',"
//                 << std::endl;

//    gp << "set term png nocrop enhanced font 'verdana,8' size 1280,600" << std::endl;
//    gp << "set output 'file.png'" << std::endl;
//    gp << "replot" << std::endl;
//    gp << "set term x11" << std::endl;

//    const clock_t end_time_total = clock();
//    double time_total = diffclock(begin_time_total, end_time_total);
//    std::cout << "Total time: " << time_total << " s." << endl;


    return 0;
}
