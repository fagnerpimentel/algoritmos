#include "../include/algoritmos.h"

Algoritimos::Algoritimos(){

}

/* mdc */

void
Algoritimos::big_low(int a, int b, int &bigger, int &lower){
    bigger = a;
    lower = b;
    if (a < b){
        bigger = b;
        lower = a;
    }
}

int
Algoritimos::mdc_iteractive(int bigger, int lower){
    int mdc = 0;
    int rest_b = 0;
    int rest_l = 0;
    for (int i = 1; i <= lower; i++) {
        rest_b = bigger % i;

        rest_l = lower % i;
        if(!rest_b & !rest_l) mdc = i;
    }
    return mdc;
}

int
Algoritimos::mdc_recursive(int bigger, int lower){
    if(lower == 0)
        return bigger;

    int rest = bigger % lower;
    return mdc_recursive(lower,rest);
}

/* sort */

void
Algoritimos::exchange(std::vector<int> &A, int i, int j){
    int aux = A[i];
    A[i] = A[j];
    A[j] = aux;
}

int
Algoritimos::partition(std::vector<int> &A, int p, int r){
    int x = A[r];
    int i = p-1;
    for (int j = p; j <= r-1; j++) {
        if(A[j] <= x){
            i = i+1;
            exchange(A,i,j);
        }
    }
    exchange(A,i+1,r);
    return i+1;
}

int
Algoritimos::randomized_partition(std::vector<int> &A, int p, int r){
    int i=  mersene.myRandom(p,r);
    exchange(A,r,i);
    return partition(A, p, r);
}

void
Algoritimos::quicksort(std::vector<int> &A, int p, int r){
    if(p < r){
        int q = partition(A, p, r);
        quicksort(A, p, q-1);
        quicksort(A, q+1, r);
    }
}

void
Algoritimos::randomized_quicksort(std::vector<int> &A, int p, int r){
    if(p < r){
        int q = randomized_partition(A, p, r);
        quicksort(A, p, q-1);
        quicksort(A, q+1, r);
    }
}

void
Algoritimos::bublesort(std::vector<int> &A){
    for (unsigned int i = 0; i <= A.size()-1; i++) {
        for (unsigned int j = A.size()-1; j >= i+1 ; j--) {
            if(A[j] < A[j-1]){
                exchange(A,j,j-1);
            }
        }
    }
}


/* search */

void
Algoritimos::initialize_single_source(std::vector<Vertex*>& G, int root_id){
    for (unsigned int i = 0; i < G.size(); i++) {
        G.at(i)->mWeight = INF;
        G.at(i)->mVertex = NIL;
    }
    G.at(root_id)->mWeight = 0;
}

int
Algoritimos::extract_min(std::vector<Vertex*>& graph, std::vector<int>& ids){
    int min_index_ids = 0;
    for (unsigned int i = 1; i < ids.size(); i++) {
        if(graph.at(ids.at(i))->mWeight < graph.at(ids.at(min_index_ids))->mWeight) min_index_ids = i;
    }
    int min_index_graph = graph.at(ids.at(min_index_ids))->mID;
    ids.erase(ids.begin()+min_index_ids);
    return min_index_graph;
}

void
Algoritimos::relax_prim(std::vector<Vertex*>& G, int u, int v, Graph w){
    if(G.at(v)->mWeight > w.getWeight(u,v)){
        G.at(v)->mWeight = w.getWeight(u,v);
        G.at(v)->mVertex = u;
    }
}

void
Algoritimos::relax_dijkstra(std::vector<Vertex*>& G, int u, int v, Graph weigths){
    if(G.at(v)->mWeight > G.at(u)->mWeight + weigths.getWeight(u,v)){
        G.at(v)->mWeight = G.at(u)->mWeight + weigths.getWeight(u,v);
        G.at(v)->mVertex = u;
    }
}


void
Algoritimos::prim(std::vector<Vertex*>& G, Graph w, int r){
    initialize_single_source(G, r);

    std::vector<int> Q;
    for (unsigned int i = 0; i < G.size(); i++) {
        Q.push_back(G.at(i)->mID);
    }

    while (!Q.empty()) {
        int u = extract_min(G, Q);

        std::vector<int> adj = w.adj(G,Q,u);
        for (unsigned int v = 0; v < adj.size(); v++) {

            if(G.at(adj.at(v))->belongs_to(G, Q)){
                relax_prim(G,G.at(u)->mID,G.at(adj.at(v))->mID,w);;
            }

        }
    }
}
void
Algoritimos::dijkstra(std::vector<Vertex*>& G, Graph w, int r){
    initialize_single_source(G, r);

    std::vector<int> S;
    std::vector<int> Q;
    for (unsigned int i = 0; i < G.size(); i++) {
        Q.push_back(G.at(i)->mID);
    }

    while (!Q.empty()) {
        int u = extract_min(G, Q);
        S.push_back(u);

        std::vector<int> adj = w.adj(G,Q,u);
        for (unsigned int v = 0; v < adj.size(); v++) {
            relax_dijkstra(G,G.at(u)->mID,G.at(adj.at(v))->mID,w);
        }

    }
}
