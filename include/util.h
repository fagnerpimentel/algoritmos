#ifndef UTIL_H
#define UTIL_H

class Vertex{
public:
    int mID;
    int mWeight;
    int mVertex;
public:
    Vertex(int id, int weight, int vertex){
        this->mID = id;
        this->mWeight = weight;
        this->mVertex = vertex;
    }
    ~Vertex(){}
    bool belongs_to(std::vector<Vertex*> graph, std::vector<int> ids){
        for (unsigned int i = 0; i < ids.size(); i++) {
            if(this->mID == graph.at(ids.at(i))->mID)
                return true;
        }
        return false;
    }
};

class Graph{
private:
    int mSize;
    std::vector<std::vector<int>> mValues;

public:
    Graph(int size, int value = INF){
        this->mSize = size;
        for (int i = 0; i < this->mSize; i++) {
            std::vector<int> v;
            for (int j = 0; j < this->mSize; j++) {
                if(i == j){
                    v.push_back(INF);
                    continue;
                }
                v.push_back(value);
            }
            mValues.push_back(v);
        }
    }

    int getWeight(int u, int v){
        return mValues.at(u).at(v);
    }
    void setWeight(int u, int v, int value){
        mValues.at(u).at(v) = value;
    }

    std::vector<int> adj(std::vector<Vertex*> G, std::vector<int> ids , int id){
        std::vector<int> adj;
        for (unsigned int i = 0; i < G.size(); i++) {
            if(this->getWeight(G.at(i)->mID, id) == INF) continue;
            adj.push_back(G.at(i)->mID);
        }
        return adj;
    }

    void print_image(std::string name){
        std::string command = "";
        command += "echo 'graph G {";

        for (int i = 0; i < this->mSize; i++) {
            for (int j = i; j < this->mSize; j++) {
                if(((this->mValues.at(i).at(j) == INF) & (i!=j))) continue;

                std::string style = "";
                (i==j) ? style = "invis" : style = "filled";
                command += std::to_string(i) +"--" + std::to_string(j)
                        + "[ label = '"+std::to_string(this->mValues.at(i).at(j))+"' style = '"+style+"'];";
            }
        }
        command += "}' | dot -Tpng >"+name+".png";
        system(command.c_str());
    }

};

#endif // UTIL_H
