#ifndef _MERSENNE_H_
#define _MERSENNE_H_

#include <random>
#include <chrono>


class Mersenne
{
private:
    std::mt19937_64 myMersenne;

    std::uniform_real_distribution<double> myUniformDistribution;

public:
    Mersenne() {
    }

    void initseed(){
    }

    //! Pseudo-random number generator using the Mersenne Twister method
    //! \return double between 0 and 1
    double myRandom() {
    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    myMersenne = std::mt19937_64(seed);
    return myUniformDistribution(myMersenne);
    }

    //! Pseudo-random number generator using the Mersenne Twister method
    //! \param min the lower bound for the random number
    //! \param max the upper bound for the random number
    //! \return double between min and max
    double myRandom(double min, double max) {
    return myRandom() * (max - min) + min;
    }

    //! Pseudo-random number generator using the Mersenne Twister method
    //! \param max the upper bound for the random number
    //! \return double between 0 and max
    double myRandom(double max) {
    return myRandom() * max;
    }
};



#endif /* _MERSENNE_H */
