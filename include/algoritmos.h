#ifndef ALGORITMOS_H
#define ALGORITMOS_H

#define INF 100000
#define NIL -1

#include <vector>
#include <iostream>

#include "mersenne_twister.h"
#include "util.h"

class Algoritimos
{
private:
    Mersenne mersene;

public:
    Algoritimos();

private:
    void big_low(int a, int b, int &bigger, int &lower);
    void exchange(std::vector<int> &A, int i, int j);

    int partition(std::vector<int> &A, int p, int r);
    int randomized_partition(std::vector<int> &A, int p, int r);

    void initialize_single_source(std::vector<Vertex*>& G, int root_id);
    int extract_min(std::vector<Vertex*>& G, std::vector<int>& ids);
    void relax_prim(std::vector<Vertex*>& G, int u, int v, Graph w);
    void relax_dijkstra(std::vector<Vertex*>& G, int u, int v, Graph w);


public:

    /* mdc */
    int mdc_iteractive(int bigger, int lower);
    int mdc_recursive(int bigger, int lower);

    /* sort */
    void quicksort(std::vector<int> &A, int p, int r);
    void randomized_quicksort(std::vector<int> &A, int p, int r);
    void bublesort(std::vector<int> &A);

    /* search */
    void prim(std::vector<Vertex*>& G, Graph w, int r);
    void dijkstra(std::vector<Vertex*>& G, Graph w, int r);

};

#endif // ALGORITMOS_H
