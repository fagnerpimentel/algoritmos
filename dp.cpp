#include <iostream>
#include <vector>

#include "include/mersenne_twister.h"

//#include <boost/tuple/tuple.hpp>
//#include "../gnuplot-iostream/gnuplot-iostream.h"

#define MAX_DIM 100
#define MAX_VALUE MAX_DIM*MAX_DIM*MAX_DIM

using namespace std;

void print_tables(vector<vector<int>> m, vector<vector<int>> s, int num_matrizes){
    cout << "m:" << endl;
    for (int i = 0; i < num_matrizes; ++i) {
        for (int j = 0; j < num_matrizes; ++j) {
            cout << m.at(i).at(j) << ",";
        }
        cout << endl;
    }
    cout << "s:" << endl;
    for (int i = 0; i < num_matrizes; ++i) {
        for (int j = 0; j < num_matrizes; ++j) {
            cout << s.at(i).at(j) << ",";
        }
        cout << endl;
    }
    cin.get();
}

static double diffclock(clock_t begin,clock_t end)
{
    double diffticks=end-begin;
    double diffms=(diffticks)/(CLOCKS_PER_SEC);
    return diffms;
}

void matrix_chain_order(vector<int> p, vector<vector<int>>& m, vector<vector<int>>& s){
    int n = p.size()-1;
    for (int l = 1; l < n; l++) {
        for (int i = 0; i < n - l; i++) {
            int j = i + l;
            m.at(i).at(j) = MAX_VALUE;
            for (int k = i; k < j; k++) {
                int cost = m.at(i).at(k) + m.at(k+1).at(j) +
                        p.at(i)*p.at(k+1)*p.at(j+1);
                if (cost < m.at(i).at(j)) {
                    m.at(i).at(j) = cost;
                    s.at(i).at(j) = k;
                }
            }
        }
    }
}

int main()
{

//    // gnuplot
//    Gnuplot gp("tee plot.gp | gnuplot -persist");
//    std::vector<std::pair<double, double> > xy_pts_A;
//    gp << "set key left top" << std::endl;


    Mersenne mersene;

    // params
    int max_matriz_qtde = 500;

    // matriz sizes
    vector<int> matriz_dimensions;

    int matriz_qtde = 6;
    matriz_dimensions.push_back(30);
    matriz_dimensions.push_back(35);
    matriz_dimensions.push_back(15);
    matriz_dimensions.push_back(5);
    matriz_dimensions.push_back(10);
    matriz_dimensions.push_back(20);
    matriz_dimensions.push_back(25);


//    for (int matriz_qtde = 2; matriz_qtde < max_matriz_qtde; matriz_qtde++) {

        // init dimensions
//        matriz_dimensions.clear();
//        for (int i = 0; i <= matriz_qtde; i++) {
//            int dimension = mersene.myRandom(1,MAX_DIM);
//            matriz_dimensions.push_back(dimension);
//        }
        // init tables m and s
        vector<vector<int>> m;
        vector<vector<int>> s;
        for (int i = 0; i < matriz_qtde; i++) {
            vector<int> v;
            for (int j = 0; j < matriz_qtde; j++) {
                v.push_back(0);
            }
            m.push_back(v);
            s.push_back(v);
        }

        const clock_t begin_time = clock();

        matrix_chain_order(matriz_dimensions, m, s);

        const clock_t end_time = clock();
        double time = diffclock(begin_time, end_time);
        print_tables(m,s,matriz_qtde);
        std::cout << "Time: " << time << " s." << endl;


//        xy_pts_A.push_back(std::make_pair(matriz_qtde, time));
//        gp << "plot" << gp.file1d(xy_pts_A) << "with lines title 'Dynamic programming',"
//                     << std::endl;

//    }

//    gp << "set term png nocrop enhanced font 'verdana,8' size 1280,600" << std::endl;
//    gp << "set output 'file.png'" << std::endl;
//    gp << "replot" << std::endl;
//    gp << "set term x11" << std::endl;

    return 0;
}
